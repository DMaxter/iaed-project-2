/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * Header for Useful Functions
 */
#ifndef __UTILS__
#define __UTILS__ 1
#include "task.h"

#define STR_BUFF 100+1
#define STR_STEP 100
#define MAX_CMD 8+1
#define MAX_STR 8000+1
/* Due to architecture issues, C handles natively with 64bits for numbers
log(2^64) is approximately 20, so the maximum number of digits of a 64bit
number is 20*/
#define ULONG_DIG 20+1

/* Clean one line from stdin */
void cleanBuffer();
/* Read a command from stdin */
int getCommand(char *command);
/* Read a string from stdin and put it on dest (starting and ending with '"') */
int getString(char **dest);
/* Read a number of stdin */
unsigned long getNumber(unsigned long *pt);
/* Does nothing */
void doNothing(void **a);

#endif
