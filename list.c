/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * Circular Double Linked Lists
 */
#include <stdio.h>
#include <stdlib.h>
#include "list.h"
#include "task.h"

/* Function: createList
 * --------------------
 * Initialize the pointer to the head of a list
 *
 * head: address of the pointer to the head of a list
 */
void createList(List **head){
	*head = NULL;
}

/* Function: clearList
 * -------------------
 * Remove all elements from a list
 *
 * head: address of the pointer to the head of a list
 */
void clearList(List **head){
	List *p;
	List *aux;

	if(*head == NULL){
		return;
	}

	p = (*head)->next;

	while(p != *head){
		aux = p->next;
		free(p);
		p = NULL;
		p = aux;
	}

	free(*head);
	*head = NULL;
}

/* Function: addElemList
 * ---------------------
 * Add task to the given list
 *
 * head: address of the pointer to the head of a list
 * item: pointer to the task to add
 */
void addElemList(List **head, Task *item){
	List *p;
	
	if(*head == NULL){
		*head = (List *) malloc(sizeof(List));
		(*head)->prev = (*head)->next = *head;
		(*head)->item = item;
	}else{
		p = (List *) malloc(sizeof(List));
		p->next = *head;
		p->prev = (*head)->prev;
		p->prev->next = p->next->prev = p;
		p->item = item;
	}
}

/* Function: isEmptyList
 * ---------------------
 * Check if a list is empty
 *
 * head: pointer to the head of a list
 *
 * returns: true if head is equal to NULL
 *			false if head is different from NULL
 */
int isEmptyList(List *head){
	return head == NULL;
}

/* Function: searchList
 * --------------------
 * Search for task, with the given id, in a list
 *
 * head: pointer to the head of a list
 * id: id of the task to search for
 *
 * returns: NULL if a task with the given id is not found
 *			the pointer to the task with the given id
 */
Task *searchList(List *head, unsigned long id){
	List *p = head;

	if(isEmptyList(p)){
		return NULL;
	}

	while(p->next != head){
		if(getTaskId(p->item) == id){
			return p->item;
		}
		
		p = p->next;
	}

	return NULL;
}

/* Function: removeElemList
 * ------------------------
 * Remove task, with the given id, from a list
 *
 * head: address of the pointer to the head of a list
 * id: id of the task to remove
 */
void removeElemList(List **head, unsigned long id){
	List *p = *head;

	if(p == NULL){
		return;
	}

	if(getTaskId(p->item) == id){
		if(p->prev == p){
			*head = NULL;
		}else{
			*head = p->prev;
			(*head)->next = p->next;
			p->next->prev = *head;
		}

		free(p);
		p = NULL;
	}else{
		while(getTaskId(p->item) != id){
			if(p->prev == *head){
				return;
			}

			p = p->prev;
		}

		p->next->prev = p->prev;
		p->prev->next = p->next;
		
		free(p);
	}
}

/* Function: printList
 * -------------------
 * Print all tasks in a list
 *
 * head: pointer to the head of a list
 */
void printList(List *head){
	List *p = head;

	if(head == NULL){
		return;
	}

	do{
		printf(" %lu", getTaskId(p->item));
		p = p->next;
	}while(p != head);
}

/* Function: nextElemList
 * ----------------------
 * Get the following element of the head of a list
 *
 * head: pointer to the head of a list
 *
 * returns: the pointer to the following element of the head
 */
List *nextElemList(List *head){
	return head->next;
}

/* Function: prevElemList
 * ----------------------
 * Get the previous element of the head of a list
 *
 * head: pointer to the head of a list
 *
 * returns: the pointer to the previous element of the head
 */
List *prevElemList(List *head){
	return head->prev;
}

/* Function: itemElemList
 * ----------------------
 * Get the item stored in the current element of a list
 *
 * elem: pointer to an element of a list
 *
 * returns: the pointer the task stored in the given element
 */
Task *itemElemList(List *elem){
	return elem->item;
}
