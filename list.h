/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * Header for Circular Double Linked Lists
 */
#ifndef __LIST_TASKS__
#define __LIST_TASKS__ 1

typedef struct list List;

#include "task.h"

struct list{
	Task *item;
	struct list *prev;
	struct list *next;
};

/* Initialize the pointer to the head of a list */
void createList(List **head);
/* Remove all elements from a list */
void clearList(List **head);
/* Add task to the given list */
void addElemList(List **head, Task *item);
/* Check if a list is empty */
int isEmptyList(List *head);
/* Search for task, with the given id, in a list */
Task *searchList(List *head, unsigned long id);
/* Remove task, with the given id, from a list */
void removeElemList(List **head, unsigned long id);
/* Print all tasks in a list */
void printList(List *head);
/* Get the following element of the head of a list */
List *nextElemList(List *head);
/* Get the previous element of the head of a list */
List *prevElemList(List *head);
/* Get the item stored in the current element of a list */
Task *itemElemList(List *elem);

#endif
