/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * Useful Functions 
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "utils.h"
#include "task.h"

static char buffer[MAX_STR];

/* Function: cleanBuffer
 * ---------------------
 * Clean one line from stdin
 */
void cleanBuffer(){
	char c;

	do{
		c = getchar();
	}while(c != '\n');
}

/* Function: getCommand
 * --------------------
 * Read a command from stdin
 *
 * command: command destination
 *
 * returns: 0 if input ended with ' '
 *			1 if input ended with '\n'
 *			2 if input was incorrect
 */
int getCommand(char *command){
	int i = 0;
	char c;

	while(i < MAX_CMD){
		c = getchar();
		
		if(c  == ' ' || c == '\n'){
			*(command + i) = '\0';
			return c == '\n' ? 1 : 0;
		}

		*(command+i) = c;
		i++;
	}

	*(command+i) = '\0';

	cleanBuffer();

	return 2;
}

/* Function: getString
 * -------------------
 * Read a string from stdin and put it on dest (starting and ending with '"')
 *
 * dest: string destination
 *
 * returns: 0 if input ended with ' '
 *			1 if input ended with '\n'
 *			2 if input was incorrect
 */
int getString(char **dest){
	char c;
	int i = 1;
		
	c = getchar();
	if(c != '"'){
		cleanBuffer();
		return 1;
	}

	buffer[0] = c;

	while(i < MAX_STR-1){
		c = getchar();
		
		if(c == '\n'){
			buffer[i] = '\0';
			return 2;
		}

		buffer[i] = c;
		i++;

		if(c == '"'){
			break;
		}
	}

	if(c != '"'){
		cleanBuffer();
	}

	buffer[i] = '\0';

	*dest = malloc(sizeof(char)*(strlen(buffer)+1));
	strcpy(*dest, buffer);

	c = getchar();

	return c == '\n' ? 1 : 0;
}

/* Function: getNumber
 * -------------------
 * Read a number from stdin
 *
 * pt: pointer to variable that will store the result
 *
 * returns: 0 if input ended with ' '
 *			1 if input ended with '\n'
 *			2 if input was no number
 */
unsigned long getNumber(unsigned long *pt){
	char *trash = NULL;
	int i = 0, j;
	char c;

	while(i < ULONG_DIG){
		c = getchar();

		if(c  == ' ' || c == '\n'){
			break;
		}
		
		buffer[i] = c;
		i++;
	}

	buffer[i] = '\0';

	if('0' <= c && c <= '9'){
		cleanBuffer();
		return 2;
	}

	for(j = 0; j < i; j++){
		if(buffer[j] < '0' || buffer[j] > '9'){
			if(c != '\n'){
				cleanBuffer();
			}

			return 2;
		}
	}

	*pt = strtoul(buffer, &trash, 10);

	if(*pt == 0){
		if(c != '\n'){
			cleanBuffer();
		}

		return 2;
	}

	return c == '\n' ? 1: 0;
}

/* Function: doNothing
 * -------------------
 * Does nothing
 *
 * a: address of pointer to something
 */
void doNothing(void **a){
	return;
}
