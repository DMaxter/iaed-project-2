/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * Header for User Interface
 */

#include "bst.h"
#include "list.h"

/* Add task to tree (and to program) */
void commandAdd(Tree **tree, List **tasklist, short *validPath, short flag);
/* Print all tasks' id, description, duration and dependencies 
(in case validPath is different from zero, print early and late start) */
void commandDuration(List *tasklist, short validPath, short flag);
/* Print task dependents (subordinate tasks) */
void commandDepend(Tree *tree, short flag);
/* Remove task from tree (and from program) */
void commandRemove(Tree **tree, List **tasklist, short *validPath, short flag);
/* Calculate early and late start for each task and print critical path */
void commandPath(List *tasklist, short *validPath, short flag);
