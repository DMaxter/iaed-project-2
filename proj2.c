/* Name: Daniel Maximiano Matos 
 * Number: 89429
 *
 * Command selection
 * Program main data structures
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "bst.h"
#include "list.h"
#include "menu.h"
#include "task.h"
#include "utils.h"

int main(){
	/* List containing pointers to the tasks, sorted by insertion order */
	List *taskID = NULL;
	/* Flag to determine whether the path is stil valid or not */
	short validPath = 0;
	/* Flag to determine whether the input is valid or not */
	short flag = 0;
	char command[MAX_CMD];
	/* BST */
	Tree *tree = NULL;

	createList(&taskID);
	createTree(&tree);

	/* Command selector */
	do{
		flag = getCommand(command);

		if(strcmp(command, "add") == 0){
			commandAdd(&tree, &taskID, &validPath, flag);
		}else if(strcmp(command, "duration") == 0){
			commandDuration(taskID, validPath, flag);
		}else if(strcmp(command, "depend") == 0){
			commandDepend(tree, flag);
		}else if(strcmp(command, "remove") == 0){
			commandRemove(&tree, &taskID, &validPath, flag);
		}else if(strcmp(command, "path") == 0){
			commandPath(taskID, &validPath, flag);
		}else if(strcmp(command, "exit") == 0){
			if(flag == 0){
				printf("illegal arguments\n");
				cleanBuffer();
				/* In case input is "exit " (or similar), program won't quit */
				command[0] = ' ';
			}

			continue;
		}else{
			printf("illegal arguments\n");
		}
	}while(strcmp(command, "exit") != 0);
	
	/* Delete the main data structures used in the program */
	clearTree(&tree, removeTask);
	clearList(&taskID);

	return 0;
}
