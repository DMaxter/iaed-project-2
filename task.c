/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * Tasks
 */
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include "bst.h"
#include "task.h"

/* Function: compareTasks
 * ----------------------
 * Compare two tasks
 *
 * a: first task
 * b: second task
 *
 * returns: -1 if a < b
 *			0 if a == b
 *			1 if a > b
 */
int compareTasks(void *a, void *b){
	Task *item1 = (Task *) a;
	Task *item2 = (Task *) b;

	if(item1->id == item2->id){
		return 0;
	}

	return item1->id > item2->id ? 1 : -1;
}

/* Function: compareTaskId
 * -----------------------
 * Compare a task id  with an unsigned long
 *
 * a: pointer to an id of a task
 * b: pointer to task
 *
 * returns: -1 if a < b
 *			0 if a == b
 *			1 if a > b
 */
int compareTaskId(void *a, void *b){
	unsigned long item1 = *((unsigned long *) a);
	Task *item2 = (Task *) b;

	if(item1 == item2->id){
		return 0;
	}

	return item1 > item2->id ? 1 : -1;
}

/* Function: compareTaskDuration
 * -----------------------------
 * Compare the duration of a task with an unsigned long
 *
 * a: pointer to a duration of a task
 * b: pointer to a task
 *
 * returns: -1 if a < b
 *			0 if a == b
 *			1 if a > b
 */
int compareTaskDuration(void *a, void *b){
	unsigned long item1 = *((unsigned long *) a);
	Task *item2 = (Task *) b;

	if(item1 == item2->duration){
		return 0;
	}

	return item1 > item2->duration ? 1 : -1;
}

/* Function: createTask
 * --------------------
 * Initialize the pointer to a task
 *
 * task: address of the pointer to a task
 */
void createTask(Task **task){
	*task = (Task *) malloc(sizeof(Task));
	(*task)->description = NULL;
	createList(&((*task)->dependencies));
	createList(&((*task)->subordinates));
}

/* Function: setTaskId
 * -------------------
 * Set the id of a task
 *
 * task: pointer to a task
 * id: task id
 */
void setTaskId(Task *task, unsigned long id){
	task->id = id;
}

/* Function: getTaskId
 * -------------------
 * Get the id of a task
 *
 * task: pointer to a task
 *
 * returns: the given task's id
 */
unsigned long getTaskId(Task *task){
	return task->id;
}

/* Function: setTaskDescription
 * ----------------------------
 * Set the description of a task
 *
 * task: pointer to a task
 * description: pointer to a string
 */
void setTaskDescription(Task *task, char *description){
	task->description = description;
}

/* Function: getTaskDescription
 * ----------------------------
 * Get the description of a task
 *
 * task: pointer to a task
 *
 * returns: the pointer to the given task's description
 */
char *getTaskDescription(Task *task){
	return task->description;
}

/* Function: setTaskDuration
 * -------------------------
 * Set the duration of a task
 *
 * task: pointer to a task
 * duration: task duration
 */
void setTaskDuration(Task *task, unsigned long duration){
	task->duration = duration;
}

/* Function: getTaskDuration
 * -------------------------
 * Get the duration of a task
 *
 * task: pointer to a task
 *
 * returns: the given task's duration
 */
unsigned long getTaskDuration(Task *task){
	return task->duration;
}

/* Function: getTaskEStart
 * -----------------------
 * Get the early start of a task
 *
 * task: pointer to a task
 *
 * returns: the given task's early start
 */
unsigned long getTaskEStart(Task *task){
	return task->earlyStart;
}

/* Function: getTaskLStart
 * -----------------------
 * Get the late start of a task
 *
 * task: pointer to a task
 *
 * returns: the given task's late start
 */
unsigned long getTaskLStart(Task *task){
	return task->lateStart;
}

/* Function: addTaskDependency
 * ---------------------------
 * Add dependency to a task
 *
 * task: pointer to a task
 * dependency: pointer to a dependency
 */
void addTaskDependency(Task *task, Task* dependency){
	addElemList(&(task->dependencies), dependency);
	addElemList(&(dependency->subordinates), task);
}

/* Function: removeTaskDependencies
 * --------------------------------
 * Clear all task's dependencies
 *
 * task: pointer to a task
 */
void removeTaskDependencies(Task *task){
	Task *dependency;

	while(!isEmptyList(task->dependencies)){
		dependency = itemElemList(task->dependencies);
		removeElemList(&(dependency->subordinates), task->id);
		removeElemList(&(task->dependencies), dependency->id);
	}
}

/* Function: getTaskDependencies
 * -----------------------------
 * Get all dependencies of a task
 *
 * task: pointer to a task
 *
 * returns: the pointer to the list of dependencies of the given task
 */
List *getTaskDependencies(Task *task){
	return task->dependencies;
}

/* Function: getTaskSubordinates
 * -----------------------------
 * Get all dependents of a task
 *
 * task: pointer to a task
 *
 * returns: the pointer to the list of dependents of the given task
 */
List *getTaskSubordinates(Task *task){
	return task->subordinates;
}

/* Function: removeTask
 * --------------------
 * Remove a task
 *
 * ptr: pointer to a task
 */
void removeTask(void **ptr){
	Task **task = (Task **) ptr;

	if((*task)->description != NULL){
		free((*task)->description);
		
		(*task)->description = NULL;
	}

	clearList(&((*task)->dependencies));
	clearList(&((*task)->subordinates));

	free(*task);

	*task = NULL;
}

/* Function: calcEarlyStart
 * ------------------------
 * Calculate early start of a task
 *
 * task: pointer to a task
 */
void calcEarlyStart(Task *task){
	unsigned long maxES = 0;
	unsigned long tmpES;
	List *elem;
	Task *tmpTask;

	if(isEmptyList(task->dependencies)){
		task->earlyStart = 0;
		return;
	}

	elem = task->dependencies;

	do{
		tmpTask = itemElemList(elem);
		tmpES = tmpTask->duration + tmpTask->earlyStart;

		if(tmpES > maxES){
			maxES = tmpES;
		}

		elem = nextElemList(elem);
	}while(elem != task->dependencies);

	task->earlyStart = maxES;
}

/* Function: calcLateStart
 * -----------------------
 * Calculate late start of a task
 *
 * task: pointer to a task
 * projDuration: duration of the project
 */
void calcLateStart(Task *task, unsigned long projDuration){
	unsigned long minLS = ULONG_MAX;
	unsigned long tmpLS;
	List *elem;
	Task *tmpTask;

	if(isEmptyList(task->subordinates)){
		task->lateStart = projDuration - task->duration;
		return;
	}

	elem = task->subordinates;

	do{
		tmpTask = itemElemList(elem);
		tmpLS = tmpTask->lateStart;

		if(tmpLS < minLS){
			minLS = tmpLS;
		}

		elem = nextElemList(elem);
	}while(elem != task->subordinates);

	task->lateStart = minLS - task->duration;
}

/* Function: printTask
 * -------------------
 * Print a task
 *
 * task: pointer to a task
 * validPath: flag that determines if early and late start are still valid
 */
void printTask(Task *task, short validPath){
	printf("%lu %s %lu", task->id,
							task->description,
							task->duration);
	if(validPath){
		if(task->earlyStart == task->lateStart){
			printf(" [%lu CRITICAL]", task->earlyStart);
		}else{
			printf(" [%lu %lu]", task->earlyStart, task->lateStart);
		}
	}

	printList(getTaskDependencies(task));
	putchar('\n');
}
