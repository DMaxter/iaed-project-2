/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * Header for Tasks
 */
#ifndef __TASK__
#define __TASK__ 1

#include "bst.h"

typedef struct task Task;

#include "list.h"

struct task{
	unsigned long id;
	char *description;
	unsigned long duration;	
	unsigned long earlyStart;
	unsigned long lateStart;
	List *dependencies;
	List *subordinates;
};

/* Compare two tasks */
int compareTasks(void *, void *);
/* Compare a task id  with an unsigned long */
int compareTaskId(void *, void *);
/* Compare the duration of a task with an unsigned long */
int compareTaskDuration(void *, void *);
/* Initialize the pointer to a task */
void createTask(Task **task);
/* Set the id of a task */
void setTaskId(Task *task, unsigned long id);
/* Get the id of a task */
unsigned long getTaskId(Task *task);
/* Set the description of a task */
void setTaskDescription(Task *task, char *description);
/* Get the description of a task */
char *getTaskDescription(Task *task);
/* Set the duration of a task */
void setTaskDuration(Task *task, unsigned long duration);
/* Get the duration of a task */
unsigned long getTaskDuration(Task *task);
/* Get the early start of a task */
unsigned long getTaskEStart(Task *task);
/* Get the late start of a task */
unsigned long getTaskLStart(Task *task);
/* Add dependency to a task */
void addTaskDependency(Task *task, Task *dependency);
/* Clear all task's dependencies */
void removeTaskDependencies(Task *task);
/* Get all dependencies of a task */
List *getTaskDependencies(Task *task);
/* Get all dependents of a task */
List *getTaskSubordinates(Task *task);
/* Remove a task */
void removeTask(void **task);
/* Calculate early start of a task */
void calcEarlyStart(Task *task);
/* Calculate late start of a task */
void calcLateStart(Task *task, unsigned long projDuration);
/* Print a task */
void printTask(Task *task, short validPath);

#endif
