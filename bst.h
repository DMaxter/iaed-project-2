/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * Header for AVL Binary Search Trees
 */
#ifndef __BST__
#define __BST__ 1

typedef struct tree{
	void *item;
	int height;
	struct tree *l;
	struct tree *r;
}Tree;

/* Initialize the pointer to the root of a tree */
void createTree(Tree **root);
/* Add element to the given tree and balance it */
void addElemTree(Tree **root, void *item, int (*compare)(void *, void *));
/* Check if a tree is empty */
int isEmptyTree(Tree *root);
/* Search for item in a tree
(first parameter of compare function is always the item parameter) */
void *searchTree(Tree *root, void *item, int (*compare)(void *, void *));
/* Remove element from a tree 
(first parameter of compare function is always the item parameter) */
void removeElemTree(Tree **root, void *item, int (*compare)(void *, void *), 
					void (*delete)(void **));
/* Remove all elements from a tree */
void clearTree(Tree **root, void (*delete)(void **));

#endif
