/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * AVL Binary Search Trees (self-balancing)
 */

#include <stdlib.h>
#include "bst.h"

/* PRIVATE FUNCTIONS */

/* Function: height
 * ----------------
 * Height of the current root
 *
 * root: pointer to the root of a tree
 *
 * returns: the height of the current root
 */
static int height(Tree *root){
	if(isEmptyTree(root)){
		return 0;
	}

	return root->height;
}

/* Function: rotateL
 * -----------------
 * Perform a tree rotation to the left (for balancing)
 *
 * root: address of pointer to the root of a tree
 */
static void rotateL(Tree **root){
	int heightL, heightR, auxL, auxR;

	Tree *aux = (*root)->r;
	(*root)->r = aux->l;
	aux->l = (*root);

	heightL = height((*root)->l);
	heightR = height((*root)->r);
	(*root)->height = heightL > heightR ? heightL + 1 : heightR + 1;

	auxL = height(aux->l);
	auxR = height(aux->r);
	aux->height = auxL > auxR ? auxL + 1 : auxR + 1;

	*root = aux;
}

/* Function: rotateR
 * -----------------
 * Perform a tree rotation to the right (for balancing)
 *
 * root: address of pointer to the root of a tree
 */
static void rotateR(Tree **root){
	int heightL, heightR, auxL, auxR;

	Tree *aux = (*root)->l;
	(*root)->l = aux->r;
	aux->r = (*root);

	heightL = height((*root)->l);
	heightR = height((*root)->r);
	(*root)->height = heightL > heightR ? heightL + 1 : heightR + 1;

	auxL = height(aux->l);
	auxR = height(aux->r);
	aux->height = auxL > auxR ? auxL + 1 : auxR + 1;

	*root = aux;
}

/* Function: rotateLR
 * ------------------
 * Perform a tree rotation to the left and then to the right
 *
 * root: address of pointer to the root of a tree
 */
static void rotateLR(Tree **root){
	if(*root == NULL){
		return;
	}

	rotateL(&((*root)->l));
	rotateR(root);
}

/* Function: rotateRL
 * ------------------
 * Perform a tree rotation to the right and then to the left
 *
 * root: address of pointer to the root of a tree
 */
static void rotateRL(Tree **root){
	if(*root == NULL){
		return;
	}

	rotateR(&((*root)->r));
	rotateL(root);
}

/* Function: maxTree
 * -----------------
 * Reach the maximum value stored in the given tree
 * 
 * root: pointer to the root of a tree
 *
 * returns: the pointer to the maximum element of the given tree
 */
static Tree *maxTree(Tree *root){
	if(root == NULL || root->r == NULL){
		return root;
	}

	return maxTree(root->r);
}

/* Function: balance
 * -----------------
 * Calculate the difference of heights of the current root's subtrees
 *
 * root: pointer to the root of a tree
 *
 * returns: the difference between the heights of the root's subtrees
 */
static int balance(Tree *root){
	if(isEmptyTree(root)){
		return 0;
	}

	return height(root->l) - height(root->r);
}

/* Function: balanceTree
 * ---------------------
 * Balance a tree (if unbalanced)
 *
 * root: address of the pointer to the root of a tree
 */
static void balanceTree(Tree **root){
	int balanceFactor, heightL, heightR;
	
	if(isEmptyTree(*root)){
		return;
	}

	balanceFactor = balance(*root);

	if(balanceFactor > 1){
		if(balance((*root)->l) >= 0){
			rotateR(root);
		}else{
			rotateLR(root);
		}
	}else if(balanceFactor < -1){
		if(balance((*root)->r) <= 0){
			rotateL(root);
		}else{
			rotateRL(root);
		}
	}else{
		heightL = height((*root)->l);
		heightR = height((*root)->r);
		(*root)->height = heightL > heightR ? heightL + 1 : heightR + 1;
	}
}

/* PUBLIC FUNCTIONS */

/* Function: createTree
 * --------------------
 * Initialize the pointer to the root of a tree
 *
 * root: address of the pointer to the root of a tree
 */
void createTree(Tree **root){
	*root = NULL;
}

/* Function: addElemTree
 * ---------------------
 * Add element to the given tree and balance it
 *
 * root: address of the pointer to the root of a tree
 * item: value to add
 * compare: comparision function (first element is ALWAYS the given item)
 */
void addElemTree(Tree **root, void *item, int (*compare)(void *, void *)){
	if(isEmptyTree(*root)){
		*root = malloc(sizeof(Tree));
		(*root)->item = item;
		(*root)->height = 1;
		(*root)->l = (*root)->r = NULL;
	}else if(compare(item, (*root)->item) < 0){
		addElemTree(&((*root)->l), item, compare);
	}else{
		addElemTree(&((*root)->r), item, compare);
	}

	balanceTree(root);
}

/* Function: isEmptyTree
 * ---------------------
 * Check if a tree is empty
 *
 * root: root of a tree
 *
 * returns: true if tree is empty
 *			false if tree is not empty
 */
int isEmptyTree(Tree *root){
	return root == NULL;
}

/* Function: searchTree
 * --------------------
 * Search for item in a tree
(first parameter of compare function is always the item parameter)
 *
 * root: root of a tree
 * item: value to compare
 * compare: comparision function (first element is ALWAYS the given item)
 *
 * returns: NULL if item is not found in the given tree
 *			the pointer to the given item
 */
void *searchTree(Tree *root, void *item, int (*compare)(void *, void *)){
	if(isEmptyTree(root)){
		return NULL;
	}else if(compare(item, root->item) == 0){
		return root->item;
	}else if(compare(item, root->item) < 0){
		return searchTree(root->l, item, compare);
	}else{
		return searchTree(root->r, item, compare);
	}
}

/* Function: removeElemTree
 * ------------------------
 * Remove element from a tree 
(first parameter of compare function is always the item parameter)
 *
 * root: address of the pointer to the root of a tree
 * item: value to compare (and remove)
 * compare: comparision function (first element is AlWAYS the given item)
 * delete: delete function for items in the tree
 */
void removeElemTree(Tree **root, void *item, int (*compare)(void *, void *),
					void (*delete)(void **)){
	if(isEmptyTree(*root)){
		return;
	}else if(compare(item, (*root)->item) < 0){
		removeElemTree(&((*root)->l), item, compare, delete);
	}else if(compare(item, (*root)->item) > 0){
		removeElemTree(&((*root)->r), item, compare, delete);
	}else{
		if(!isEmptyTree((*root)->l) && !isEmptyTree((*root)->r)){
			Tree *aux = maxTree((*root)->l);
			void *x = (*root)->item;
			(*root)->item = aux->item;
			aux->item = x;
			removeElemTree(&((*root)->l), item, compare, delete);
		}else{
			Tree *aux = *root;
			
			if(isEmptyTree((*root)->l) && isEmptyTree((*root)->r)){
				*root = NULL;
			}else if(isEmptyTree((*root)->l)){
				*root = (*root)->r;
			}else{
				*root = (*root)->l;
			}

			delete(&(aux->item));
			free(aux);
			aux = NULL;
		}
	}

	balanceTree(root);
}

/* Function: clearTree
 * -------------------
 * Remove all elements from a tree
 *
 * root: address of the pointer to the root of a tree
 * delete: delete function for items in the tree
 */
void clearTree(Tree **root, void (*delete)(void **)){
	if(*root == NULL){
		return;
	}else if((*root)->l == NULL && (*root)->r == NULL){
		delete(&((*root)->item));
		free(*root);
		*root = NULL;
	}else if((*root)->l == NULL){
		clearTree(&((*root)->r), delete);
		delete(&((*root)->item));
		free(*root);
		*root = NULL;
	}else if((*root)->r == NULL){
		clearTree(&((*root)->l), delete);
		delete(&((*root)->item));
		free(*root);
		*root = NULL;
	}else{
		clearTree(&((*root)->l), delete);
		clearTree(&((*root)->r), delete);
		delete(&((*root)->item));
		free(*root);
		*root = NULL;
	}
}
