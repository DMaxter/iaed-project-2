/* Name: Daniel Maximiano Matos
 * Number: 89429
 *
 * User Interface
 */
#include <stdio.h>
#include "bst.h"
#include "list.h"
#include "task.h"
#include "utils.h"

/* Auxiliary variables defined as static (and global) to save some execution 
	time and memory */
static Task *task = NULL;
static void *auxptr = NULL;
static unsigned long auxvar, auxvar2;
static unsigned long projDuration;

/* Function: commandAdd
 * --------------------
 * Add task to tree (and to program)
 *
 * tree: pointer to the pointer of the tree
 * tasklist: pointer to the pointer of the list that contains all tasks sorted
by insertion order
 * validPath: flag that determines if the current critical path is still valid
 * flag: value returned by the command getter
 */
void commandAdd(Tree **tree, List **tasklist, short *validPath, short flag){
	Tree *depTree = NULL;
	if(flag == 1){
		printf("illegal arguments\n");
		return;
	}

	createTask(&task);

	/* Get ID */
	auxvar2 = getNumber(&auxvar);
	if(auxvar2 != 0){
		printf("illegal arguments\n");
		removeTask((void **) &task);
		return;
	}
	
	auxptr = searchTree(*tree, &auxvar, compareTaskId);
	if(auxptr != NULL){
		printf("id already exists\n");
		removeTask((void **) &task);

		if(auxvar2 == 0){
			cleanBuffer();
		}

		return;
	}

	setTaskId(task, auxvar);

	/* Get Description */
	auxvar2 = getString((char **) &auxptr);
	if(auxvar2 != 0){
		printf("illegal arguments\n");
		removeTask((void **) &task);
		return;
	}

	setTaskDescription(task, auxptr);

	/* Get Duration */
	auxvar2 = getNumber(&auxvar);
	if(auxvar2 == 2){
		printf("illegal arguments\n");
		removeTask((void **) &task);
		return;
	}

	setTaskDuration(task, auxvar);
	
	if(auxvar2 == 0){
		createTree(&depTree);
		/* Get Dependencies */
		do{
			auxvar2 = getNumber(&auxvar);
			auxptr = searchTree(*tree, &auxvar, compareTaskId);

			if(auxvar2 == 2){
				printf("illegal arguments\n");
				removeTaskDependencies(task);
				removeTask((void **) &task);
			}else if(auxptr == NULL){
				printf("no such task\n");
				removeTaskDependencies(task);
				removeTask((void **) &task);

				if(auxvar2 == 0){
					cleanBuffer();
				}

				break;
			}else{
				if(searchTree(depTree, &auxvar, compareTaskId) == NULL){ 
					addElemTree(&depTree, auxptr, compareTaskId);
					addTaskDependency(task, auxptr);
				}
			}
		}while(auxvar2 == 0);
		clearTree(&depTree, doNothing);
	}

	/* If all went well add task to BST */
	if(auxvar2 == 1 && auxptr != NULL){
		auxvar = getTaskId(task);
		addElemTree(tree, task, compareTaskId);	
		
		addElemList(tasklist, task);	
			
		/* Invalidate path */	
		*validPath = 0;
	}
}

/* Function: commandDuration
 * -------------------------
 * Print all tasks' id, description, duration and dependencies
(in case validPath is different from zero, print early and late start)
 *
 * tasklist: pointer to the list containing all tasks sorted by insertion order
 * validPath: flag that determines if the last early and late start calculated
are still valid or not
 * flag: value returned by command getter
 */
void commandDuration(List *tasklist, short validPath, short flag){
	if(flag == 0){
		auxvar2 = getNumber(&auxvar);

		if(auxvar2 != 1){
			printf("illegal arguments\n");
			
			if(auxvar2 == 0){
				cleanBuffer();
			}

			return;
		}
	}else{
		auxvar = 0;
	}

	auxptr = tasklist;

	if(tasklist != NULL){
		do{
			task = itemElemList(auxptr);

			if(compareTaskDuration(&auxvar, task) <= 0){
				printTask(task, validPath);
			}

			auxptr = nextElemList(auxptr);
		}while(auxptr != tasklist);
	}
}

/* Function: commandDepend
 * -----------------------
 * Print task dependents (subordinate tasks)
 *
 * tree: pointer to the tree containing all tasks
 * flag: value returned by command getter
 */
void commandDepend(Tree *tree, short flag){
	if(flag == 1){
		printf("illegal arguments\n");
		return;
	}

	auxvar2 = getNumber(&auxvar);
	if(auxvar2 != 1){
		printf("illegal arguments\n");
		return;
	}

	task = searchTree(tree, &auxvar, compareTaskId);
	if(task == NULL){
		printf("no such task\n");
		return;
	}

	auxptr = getTaskSubordinates(task);
	if(isEmptyList(auxptr)){
		printf("%lu: no dependencies\n", getTaskId(task));
		return;
	}

	printf("%lu:", getTaskId(task));
	printList(auxptr);
	putchar('\n');
}

/* Function: commandRemove
 * -----------------------
 * Remove task from tree (and from program)
 *
 * tree: pointer to pointer of the tree
 * tasklist: pointer to pointer of the list containing all tasks sorted by
insertion order
 * validPath: pointer to the flag that determines whether critical path is still 
valid or not
 * flag: value returned from command getter
 */
void commandRemove(Tree **tree, List **tasklist, short *validPath, short flag){
	if(flag == 1){
		printf("illegal arguments\n");
		return;
	}

	auxvar2 = getNumber(&auxvar);
	if(auxvar2 != 1){
		printf("illegal arguments\n");
		
		if(auxvar2 == 0){
			cleanBuffer();
		}

		return;
	}

	task = searchTree(*tree, &auxvar, compareTaskId);
	if(task == NULL){
		printf("no such task\n");
		return;
	}

	/* Check if task has no depedent tasks */
	if(!isEmptyList(getTaskSubordinates(task))){
		printf("task with dependencies\n");
		return;
	}

	/* Remove task from depents list of its dependencies */
	removeTaskDependencies(task);
	/* Remove task from BST and delete entry from tasklist */
	removeElemList(tasklist, auxvar);
	removeElemTree(tree, &auxvar, compareTaskId, removeTask);
	/* Invalidate path */
	*validPath = 0;
}

/* Function: commandPath
 * ---------------------
 * Calculate early and late start for each task and print critical path
 *
 * tasklist: pointer to the list containing all tasks sorted by insertion
order
 * validPath: flag determining if path is still valid
 * flag: value returned from command getter
 */
void commandPath(List *tasklist, short *validPath, short flag){
	if(flag == 0){
		printf("illegal arguments\n");
		cleanBuffer();
		return;
	}


	if(tasklist != NULL){
		/* If path is still valid no need to recalculate everything again */
		if(*validPath == 0){
			auxptr = tasklist;
			projDuration = 0;

			/* Calculate early start of each task */
			do{
				task = itemElemList(auxptr);
				calcEarlyStart(task); 

				auxvar = getTaskEStart(task) + getTaskDuration(task);

				if(auxvar > projDuration){
					projDuration = auxvar;
				}

				auxptr = nextElemList(auxptr);
			}while(auxptr != tasklist);

			/* Calculate late start of each task */
			auxptr = prevElemList(tasklist);

			do{
				task = itemElemList(auxptr);
				calcLateStart(task, projDuration);
				auxptr = prevElemList(auxptr);
			}while(auxptr != prevElemList(tasklist));

			/* (Re)Validate path */
			*validPath = 1;
		}

		/* Print the critical path */
		auxptr = tasklist;

		do{
			task = itemElemList(auxptr);
			auxvar = getTaskEStart(task);
			auxvar2 = getTaskLStart(task);

			if(auxvar == auxvar2){
				printTask(task, *validPath);
			}

			auxptr = nextElemList(auxptr);
		}while(auxptr != tasklist);
	}else{
		projDuration = 0;
	}

	printf("project duration = %lu\n", projDuration);
}
